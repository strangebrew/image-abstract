export type SourceImage = {
    width: number;
    height: number;
};

export type Brush = {
    shape: string;
    batchSize: number;
    min: number;
    max: number;
    colorList: string[];
    filter: number;
};

export type ChangeEvent = Event & { currentTarget: EventTarget & HTMLInputElement };

export const rgbToHex = (r: number, g: number, b: number) => {
    return (
        "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1)
    );
};

export const hexToRgb = (hex: string) => {
    const r = "0x" + hex[1] + hex[2];
    const g = "0x" + hex[3] + hex[4];
    const b = "0x" + hex[5] + hex[6];
    return [+r, +g, +b];
}

export function rgb2lab(rgb: number[]) {
    let r = rgb[0] / 255,
        g = rgb[1] / 255,
        b = rgb[2] / 255,
        x,
        y,
        z;
    r = r > 0.04045 ? Math.pow((r + 0.055) / 1.055, 2.4) : r / 12.92;
    g = g > 0.04045 ? Math.pow((g + 0.055) / 1.055, 2.4) : g / 12.92;
    b = b > 0.04045 ? Math.pow((b + 0.055) / 1.055, 2.4) : b / 12.92;
    x = (r * 0.4124 + g * 0.3576 + b * 0.1805) / 0.95047;
    y = (r * 0.2126 + g * 0.7152 + b * 0.0722) / 1.0;
    z = (r * 0.0193 + g * 0.1192 + b * 0.9505) / 1.08883;
    x = x > 0.008856 ? Math.pow(x, 1 / 3) : 7.787 * x + 16 / 116;
    y = y > 0.008856 ? Math.pow(y, 1 / 3) : 7.787 * y + 16 / 116;
    z = z > 0.008856 ? Math.pow(z, 1 / 3) : 7.787 * z + 16 / 116;
    return [116 * y - 16, 500 * (x - y), 200 * (y - z)];
};

export function deltaE(rgbA: number[], rgbB: number[]) {
    let labA = rgb2lab(rgbA);
    let labB = rgb2lab(rgbB);
    let deltaL = labA[0] - labB[0];
    let deltaA = labA[1] - labB[1];
    let deltaB = labA[2] - labB[2];
    let c1 = Math.sqrt(labA[1] * labA[1] + labA[2] * labA[2]);
    let c2 = Math.sqrt(labB[1] * labB[1] + labB[2] * labB[2]);
    let deltaC = c1 - c2;
    let deltaH = deltaA * deltaA + deltaB * deltaB - deltaC * deltaC;
    deltaH = deltaH < 0 ? 0 : Math.sqrt(deltaH);
    let sc = 1.0 + 0.045 * c1;
    let sh = 1.0 + 0.015 * c1;
    let deltaLKlsl = deltaL / 1.0;
    let deltaCkcsc = deltaC / sc;
    let deltaHkhsh = deltaH / sh;
    let i =
        deltaLKlsl * deltaLKlsl +
        deltaCkcsc * deltaCkcsc +
        deltaHkhsh * deltaHkhsh;
    return i < 0 ? 0 : Math.sqrt(i);
}

export function random(min: number, max: number) {
    return Math.floor(min + Math.random() * (max - min));
}

export function getAverageColor(x: number, y: number, w: number, h: number, ctx: CanvasRenderingContext2D) {
    let wR = 0;
    let wG = 0;
    let wB = 0;
    let wTotal = 0;
    const data = ctx.getImageData(x, y, w, h).data;
    const components = data.length;
    for (let i = 0; i < components; i += 4) {
        const r = data[i];
        const g = data[i + 1];
        const b = data[i + 2];
        const a = data[i + 3];
        const w = a / 255;
        wR += r * w;
        wG += g * w;
        wB += b * w;
        wTotal += w;
    }
    wR = (wR / wTotal) | 0;
    wG = (wG / wTotal) | 0;
    wB = (wB / wTotal) | 0;
    return [wR, wG, wB];
}