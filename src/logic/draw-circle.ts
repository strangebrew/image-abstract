import { deltaE, getAverageColor, hexToRgb, random, rgbToHex } from "./utils";
import type { Brush, SourceImage } from "./utils";

export const drawCircle = (brush: Brush, image: SourceImage, contexts: CanvasRenderingContext2D[]): void => {
    for (let i = 0; i < brush.batchSize; i++) {
        draw(brush, image, contexts);
    }
};

const draw = ({ min, max, colorList, filter}: Brush, { width, height }: SourceImage, contexts: CanvasRenderingContext2D[]) => {
    const randomColor = colorList[random(0, colorList.length)];
    const randomSize = random(min, max);
    const randomX = random(randomSize, width - randomSize);
    const randomY = random(randomSize, height - randomSize);

    const startAngle = 0;
    const endAngle = Math.PI + (Math.PI * 2) / 2;

    const sourceRGB = getAverageColor(
        randomX,
        randomY,
        randomSize * 2,
        randomSize * 2,
        contexts[0]
    );

    // Compare average color with random color
    const rgb1 = hexToRgb(randomColor);
    const result1 = deltaE(sourceRGB, rgb1);

    // IF random color is close to avaerage color, draw shape
    if (result1 <= filter) {
        contexts[1].fillStyle = randomColor;
        contexts[1].beginPath();
        contexts[1].arc(randomX, randomY, randomSize, startAngle, endAngle);
        contexts[1].fill();
    }

    // Create the average color image
    contexts[2].fillStyle = rgbToHex(sourceRGB[0], sourceRGB[1], sourceRGB[2]);
    contexts[2].fillRect(randomX, randomY, randomSize * 2, randomSize * 2);
};